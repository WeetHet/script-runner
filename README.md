# Script runner

A GUI tool that allows users to enter a script, execute it, and see its output side-by-side.
You can use Swift and Kotlin as languages.

Build with `cargo build`, run with `cargo run`.

![Screenshot](screenshots/image1.png)

