mod highlighting;

use eframe::emath::Align;
use egui::FontFamily;
use std::sync::atomic::AtomicBool;
use std::sync::{Arc, RwLock};

fn main() -> eframe::Result<()> {
    env_logger::init();
    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default()
            .with_inner_size([640.0, 480.0])
            .with_min_inner_size([320.0, 240.0]),
        ..Default::default()
    };

    eframe::run_native(
        "Script runner",
        options,
        Box::new(|cc| {
            cc.egui_ctx.set_style(egui::Style {
                visuals: egui::Visuals::dark(),
                ..Default::default()
            });
            Box::<App>::default()
        }),
    )
}

struct App {
    language: Language,
    code: String,
    timeout: f64,
    running: Arc<AtomicBool>,
    output: Arc<RwLock<Option<ExecResult>>>,
}

impl Default for App {
    fn default() -> Self {
        Self {
            language: Language::Kotlin,
            code: String::from("print(\"Hello World\")"),
            timeout: 10.0,
            running: Arc::new(AtomicBool::new(false)),
            output: Arc::new(RwLock::new(None)),
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.with_layout(egui::Layout::left_to_right(Align::Min), |ui| {
                ui.vertical(|ui| {
                    ui.horizontal(|ui| {
                        ui.label("Language");
                        ui.radio_value(&mut self.language, Language::Kotlin, "Kotlin");
                        ui.radio_value(&mut self.language, Language::Swift, "Swift");

                        if ui.button("Run").clicked()
                            && !self.running.load(std::sync::atomic::Ordering::SeqCst)
                        {
                            let running = self.running.clone();
                            let output = self.output.clone();
                            let language = self.language;
                            let code = self.code.clone();
                            let timeout = self.timeout;
                            std::thread::spawn(move || {
                                running.store(true, std::sync::atomic::Ordering::SeqCst);
                                let result = language.execute(&code, timeout);
                                running.store(false, std::sync::atomic::Ordering::SeqCst);
                                match result {
                                    Ok(code_out) => {
                                        output.write().unwrap().replace(code_out);
                                    }
                                    Err(err) => {
                                        output
                                            .write()
                                            .unwrap()
                                            .replace(ExecResult::Error(err.to_string()));
                                    }
                                }
                            });
                        }

                        if self.running.load(std::sync::atomic::Ordering::SeqCst) {
                            ui.spinner();
                        }

                        if let Some(output) = self.output.read().unwrap().as_ref() {
                            ui.spacing();

                            ui.label("Result: ");

                            const ICON_SIZE: f32 = 5.0;

                            let (_, rect) = ui
                                .allocate_space(egui::Vec2::new(2.0 * ICON_SIZE, 2.0 * ICON_SIZE));

                            ui.painter().circle_filled(
                                egui::Pos2::new(rect.left() + ICON_SIZE, rect.top() + ICON_SIZE),
                                ICON_SIZE,
                                match output {
                                    ExecResult::Success(_) => egui::Color32::GREEN,
                                    ExecResult::Error(_) => egui::Color32::RED,
                                },
                            );
                        };
                    });

                    ui.add(
                        egui::Slider::new(&mut self.timeout, 0.0..=60.0)
                            .step_by(0.1)
                            .text("Timeout")
                            .suffix("s"),
                    );

                    let mut layouter = |ui: &egui::Ui, string: &str, wrap_width: f32| {
                        let mut layout_job =
                            highlighting::highlight(ui.ctx(), string, &self.language.to_string());
                        layout_job.wrap.max_width = wrap_width;
                        ui.fonts(|f| f.layout_job(layout_job))
                    };

                    egui::ScrollArea::vertical().show(ui, |ui| {
                        let rows = ui.available_height()
                            / ui.fonts(|r| {
                                r.row_height(&egui::FontId {
                                    size: ui.style().text_styles[&egui::TextStyle::Monospace].size,
                                    family: FontFamily::Monospace,
                                })
                            });
                        ui.add(
                            egui::TextEdit::multiline(&mut self.code)
                                .font(egui::TextStyle::Monospace)
                                .code_editor()
                                .desired_width(ui.available_width() / 2.0)
                                .desired_rows((rows as usize).max(20))
                                .lock_focus(true)
                                .layouter(&mut layouter),
                        );
                    });
                });

                ui.separator();

                ui.vertical(|ui| {
                    ui.label("Output");
                    ui.separator();
                    if let Ok(output) = self.output.read() {
                        if let Some(output) = output.as_ref() {
                            ui.label(output.text());
                        }
                    }
                });
            });
        });
    }
}

#[derive(PartialEq, Clone, Copy)]
enum Language {
    Kotlin,
    Swift,
}

impl std::fmt::Display for Language {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Language::Kotlin => write!(f, "Kotlin"),
            Language::Swift => write!(f, "Swift"),
        }
    }
}

enum ExecResult {
    Success(String),
    Error(String),
}

impl ExecResult {
    fn text(&self) -> &str {
        match self {
            ExecResult::Success(text) => text,
            ExecResult::Error(text) => text,
        }
    }
}

impl Language {
    fn execute(&self, code: &str, timeout: f64) -> anyhow::Result<ExecResult> {
        use wait_timeout::ChildExt;
        let mut cmd = std::process::Command::new("env");
        match self {
            Language::Kotlin => {
                cmd.arg("kotlinc");
                cmd.arg("-script");
            }
            Language::Swift => {
                cmd.arg("swift");
            }
        };

        let temp = tempfile::tempdir()?;
        let file = temp.path().join(match self {
            Language::Kotlin => "main.kts",
            Language::Swift => "main.swift",
        });

        std::fs::write(&file, code)?;

        cmd.arg(file);
        cmd.stdout(std::process::Stdio::piped());
        cmd.stderr(std::process::Stdio::piped());

        let mut child = cmd.spawn()?;
        let timeout = std::time::Duration::from_secs_f64(timeout);
        let status_code = if let Some(status) = child.wait_timeout(timeout)? {
            status
        } else {
            child.kill()?;
            child.wait()?
        };

        if status_code.success() {
            Ok(ExecResult::Success(
                String::from_utf8_lossy(&child.wait_with_output()?.stdout).to_string(),
            ))
        } else {
            Ok(ExecResult::Error(
                String::from_utf8_lossy(&child.wait_with_output()?.stderr).to_string(),
            ))
        }
    }
}
