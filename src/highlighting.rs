// Taken from the egui_extras source code and modified to support Kotlin and Swift syntax highlighting.

use egui::text::LayoutJob;
use once_cell::sync::Lazy;
use syntect::parsing::{SyntaxDefinition, SyntaxSet};

pub fn highlight(ctx: &egui::Context, code: &str, language: &str) -> LayoutJob {
    impl egui::util::cache::ComputerMut<(&str, &str), LayoutJob> for Highlighter {
        fn compute(&mut self, (code, lang): (&str, &str)) -> LayoutJob {
            self.highlight(code, lang)
        }
    }

    type HighlightCache = egui::util::cache::FrameCache<LayoutJob, Highlighter>;

    ctx.memory_mut(|mem| mem.caches.cache::<HighlightCache>().get((code, language)))
}

const KOTLIN_SYNTAX: &str = include_str!("../languages/Kotlin.sublime-syntax");
static KOTLIN_SYNTECT_SYNTAX: Lazy<SyntaxDefinition> = Lazy::new(|| {
    SyntaxDefinition::load_from_str(KOTLIN_SYNTAX, true, Some("Kotlin"))
        .expect("Failed to load Kotlin syntax definition")
});

const SWIFT_SYNTAX: &str = include_str!("../languages/Swift.sublime-syntax");
static SWIFT_SYNTECT_SYNTAX: Lazy<SyntaxDefinition> = Lazy::new(|| {
    SyntaxDefinition::load_from_str(SWIFT_SYNTAX, true, Some("Swift"))
        .expect("Failed to load Swift syntax definition")
});

struct Highlighter {
    ps: SyntaxSet,
    ts: syntect::highlighting::ThemeSet,
}

impl Default for Highlighter {
    fn default() -> Self {
        let cache = xdg::BaseDirectories::with_prefix("internship-script-runner")
            .expect("Cannot access XDG directories")
            .create_cache_directory("syntax")
            .expect("Cannot create cache directory");

        let invalidate = std::env::var("INVALIDATE_SYNTAX_CACHE").is_ok();

        let preload = (!invalidate)
            .then(|| {
                std::fs::read(cache.join("syntax-set.bin"))
                    .ok()
                    .and_then(|bytes| bincode::deserialize::<SyntaxSet>(&bytes).ok())
            })
            .flatten();

        let check_languages = |ps: &SyntaxSet| {
            ps.find_syntax_by_name("Kotlin").is_some() && ps.find_syntax_by_name("Swift").is_some()
        };

        let ps = match preload {
            Some(ps) if check_languages(&ps) => ps,
            _ => {
                let mut ps = SyntaxSet::load_defaults_newlines().into_builder();
                ps.add(KOTLIN_SYNTECT_SYNTAX.clone());
                ps.add(SWIFT_SYNTECT_SYNTAX.clone());

                log::debug!("Saving syntax set to cache");
                let ps = ps.build();
                let bytes = bincode::serialize(&ps).expect("Failed to serialize syntax set");
                std::fs::write(cache.join("syntax-set.bin"), bytes)
                    .expect("Failed to write syntax set to cache");
                log::debug!("Saved syntax set to cache");
                ps
            }
        };

        Self {
            ps,
            ts: syntect::highlighting::ThemeSet::load_defaults(),
        }
    }
}

impl Highlighter {
    #[allow(clippy::unused_self, clippy::unnecessary_wraps)]
    fn highlight(&self, code: &str, lang: &str) -> LayoutJob {
        self.highlight_impl(code, lang).unwrap_or_else(|| {
            // Fallback:
            LayoutJob::simple(
                code.into(),
                egui::FontId::monospace(12.0),
                egui::Color32::LIGHT_GRAY,
                f32::INFINITY,
            )
        })
    }

    fn highlight_impl(&self, text: &str, language: &str) -> Option<LayoutJob> {
        use syntect::easy::HighlightLines;
        use syntect::highlighting::FontStyle;
        use syntect::util::LinesWithEndings;

        let syntax = self
            .ps
            .find_syntax_by_name(language)
            .or_else(|| self.ps.find_syntax_by_extension(language))?;

        let theme = "base16-mocha.dark";
        let mut h = HighlightLines::new(syntax, &self.ts.themes[theme]);

        use egui::text::{LayoutSection, TextFormat};

        let mut job = LayoutJob {
            text: text.into(),
            ..Default::default()
        };

        for line in LinesWithEndings::from(text) {
            for (style, range) in h.highlight_line(line, &self.ps).ok()? {
                let fg = style.foreground;
                let text_color = egui::Color32::from_rgb(fg.r, fg.g, fg.b);
                let italics = style.font_style.contains(FontStyle::ITALIC);
                let underline = style.font_style.contains(FontStyle::ITALIC);
                let underline = if underline {
                    egui::Stroke::new(1.0, text_color)
                } else {
                    egui::Stroke::NONE
                };
                job.sections.push(LayoutSection {
                    leading_space: 0.0,
                    byte_range: as_byte_range(text, range),
                    format: TextFormat {
                        font_id: egui::FontId::monospace(12.0),
                        color: text_color,
                        italics,
                        underline,
                        ..Default::default()
                    },
                });
            }
        }

        Some(job)
    }
}

fn as_byte_range(whole: &str, range: &str) -> std::ops::Range<usize> {
    let whole_start = whole.as_ptr() as usize;
    let range_start = range.as_ptr() as usize;
    assert!(whole_start <= range_start);
    assert!(range_start + range.len() <= whole_start + whole.len());
    let offset = range_start - whole_start;
    offset..(offset + range.len())
}
